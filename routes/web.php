<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
    // return json_encode(DB::table('high_check_results')
    //                     ->leftJoin('form_results','form_results.high_check_results_id','=','high_check_results.id')
    //                     ->select('high_check_results.*','form_results.id as form_results_id')
    //                     ->orderBy('high_check_results.id', 'asc')
    //                     ->groupBy('high_check_results.id')
    //                     ->get());
    // return DB::statement('select * from high_check_results');
});

Route::get('admin', function () {
    return redirect('admin/dashboard');
});
<?php 

namespace App\Services;

use Illuminate\Support\Facades\DB;

class MasterVoucherService {
  public static $tableName = 'master_vouchers';

  public static function isCountVoucherUsed($status = 'Yes')
  {
    return DB::table(self::$tableName)->where('is_used', $status)->count();
  }
}
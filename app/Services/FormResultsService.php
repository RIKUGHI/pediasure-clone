<?php 

namespace App\Services;

use Illuminate\Support\Facades\DB;

class FormResultsService {
  public static $tableName = 'form_results';

  public static function countData()
  {
    return DB::table(self::$tableName)->count();
  }
}
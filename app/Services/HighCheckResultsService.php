<?php 

namespace App\Services;

use Illuminate\Support\Facades\DB;

class HighCheckResultsService {
  public static $tableName = 'high_check_results';

  public static function countDataByType($type)
  {
    return DB::table(self::$tableName)->where('type', $type)->count();
  }

  public static function countData()
  {
    return DB::table(self::$tableName)->count();
  }
}
<div class="box box-default">
  <div class="box-header">
      @if($type == 'form_result')
    <form autocomplete="off" method="get" action="" enctype="multipart/form-data">
      @else
      <form autocomplete="off" method="get" action="{{ url('admin/high_check_results') }}" enctype="multipart/form-data">
      @endif
      <div class="row">
         <div class="col-sm-4">
            <label class="lbl">Filter range date</label>    
                  <input name="range_date" type="" class="form-control" id="range-date" autocomplete="off">
         </div>
         <div class="col-sm-3">
            <label class="lbl white">.</label><br>
              <button type="submit" id="submit" class="btn btn-md btn-primary"><i class="fa fa-search"></i> Cari</button>
              @if(g('range_date'))
                      @if($type == 'form_result')
                          <a href="{{url('admin/form_results')}}" class="btn btn-warning">Reset Filter</a>
                      @else
                          <a href="{{url('admin/high_check_results')}}" class="btn btn-warning">Reset Filter</a>
                      @endif
                  @endif
         </div>
    </div>
  </form>
  </div>
</div>

<script>
  $(function() {
      $('#range-date').daterangepicker({
          format:'YYYY-MM-DD',
          opens: 'right',
          // startDate: '10-02-2022',
          // endDate: '10-02-2022',
      });
      var dateStart = "<?php echo $dateStartFinal ?>";
      var dateEnd = "<?php echo $dateEndFinal ?>";

      if(dateStart && dateEnd){
          $("#range-date").data('daterangepicker').setStartDate(dateStart);
          $("#range-date").data('daterangepicker').setEndDate(dateEnd);;
      }
  })
</script>
@extends('crudbooster::admin_template')
@section('content')
  <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

  <div class="box box-default">
      <div class="box-header">
        <div class="row">
          <div class="col-md-6">
            <div class="row_one box-png bg-blue-old">
              <div class="box-header ui-sortable-handle">
                <div class="row">
                  <div class="col-md-8 box-title">
                    <p class="title">Total input cek tinggi dengan TV A</p>
                    <p class="number">{{number_format($totalInputA)}}</p>
                  </div>
                  <div class="col-md-12 box-title">
                    <p class="view-detail"><a href="{{asset('admin/high_check_results?filter_column%5Btype%5D=A')}}">View Detail</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="box-default box-png bg-blue-young">
              <div class="box-header ui-sortable-handle">
                <div class="row">
                  <div class="col-md-8 box-title">
                    <p class="title">Total input cek tinggi dengan TV B</p>
                    <p class="number">{{number_format($totalInputB)}}</p>
                  </div>
                  <div class="col-md-12 box-title">
                    <p class="view-detail"><a href="{{asset('admin/high_check_results?filter_column%5Btype%5D=B')}}">View Detail</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-20">
          <div class="col-md-6">
            <div class="box-default box-png bg-purple">
              <div class="box-header ui-sortable-handle">
                <div class="row">
                  <div class="col-md-8 box-title">
                    <p class="title">Total Cek Tinggi</p>
                    <p class="number">{{number_format($totalCheckHigh)}}</p>
                  </div>
                  <div class="col-md-12 box-title">
                    <p class="view-detail"><a href="{{asset('admin/high_check_results')}}">View Detail</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="box-default box-png bg-green">
              <div class="box-header ui-sortable-handle">
                <div class="row">
                  <div class="col-md-8 box-title">
                    <p class="title">Total Pengisian Form</p>
                    <p class="number">{{number_format($totalInputForm)}}</p>
                  </div>
                  <div class="col-md-12 box-title">
                    <p class="view-detail"><a href="{{asset('admin/form_results')}}">View Detail</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-20">
          <div class="col-md-6">
            <div class="box-default box-png bg-red">
              <div class="box-header ui-sortable-handle">
                <div class="row">
                  <div class="col-md-8 box-title">
                    <p class="title">Voucher Yang Sudah Digunakan</p>
                    <p class="number">{{number_format($totalVouchersUsed)}}</p>
                  </div>
                  <div class="col-md-12 box-title">
                    <p class="view-detail"><a href="{{asset('admin/master_no_voucher?filter_column%5Bis_used%5D=Yes')}}">View Detail</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="box-default box-png bg-orange">
              <div class="box-header ui-sortable-handle">
                <div class="row">
                  <div class="col-md-8 box-title">
                    <p class="title">Voucher Yang Belum Digunakan</p>
                    <p class="number">{{number_format($totalVouchers)}}</p>
                  </div>
                  <div class="col-md-12 box-title">
                    <p class="view-detail"><a href="{{asset('admin/master_no_voucher?filter_column%5Bis_used%5D=No')}}">View Detail</a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
@endsection